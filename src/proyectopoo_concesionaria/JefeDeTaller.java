/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.util.ArrayList;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosComprados;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosMantenimiento;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosPorEntregar;


/**
 *
 * @author Campoverde Diego
 */
public class JefeDeTaller extends Usuario {
    
    private static ArrayList<Solicitud> solicitudesMantenimieno = new ArrayList<>();
   
    ArrayList<String> certificacionesTecnicas = new ArrayList<>();
    
    public JefeDeTaller(String usuario, String contrasena, String nombres, String apellidos, ArrayList<String> certificacionesTecnicas) {
        super(usuario, contrasena, nombres, apellidos);
        this.certificacionesTecnicas=certificacionesTecnicas;
    }

    public JefeDeTaller() {
    }
    
    
    /**
     * Agrega el vehiculo comprado a la lista de vehiculos del usuario comprador
     */
    /*public void entregarVehiculo( Cliente c, Vehiculo v){ 
        
            vehiculosPorEntregar.remove(v);
            c.getVehiculos().add(v);
            vehiculosComprados.add(v);
        
        
    }*/
    public void entregarVehiculo( Vehiculo v){ 
        
            vehiculosPorEntregar.remove(v);
            
            vehiculosComprados.add(v);
        
        
    }
    
    /**
     * 
     */
    public void aprobarSolicitudDeMantenimiento( String usurario){
        for(Solicitud solicitudMantenimiento :solicitudesMantenimieno  ){
            if(solicitudMantenimiento.getCliente().getUsuario().equals(usuario)){
                solicitudMantenimiento.setEstado("Aprobado");
                vehiculosMantenimiento.add(solicitudMantenimiento.getVehiculo());
                vehiculosComprados.remove(solicitudMantenimiento.getVehiculo());
            }
        }
    }

    public void cambiarEstadoDeMantenimiento(Vehiculo vehiculo,int i){
        for (Vehiculo vEnMantenimiento : vehiculosMantenimiento){
            if(vEnMantenimiento.equals(vehiculo)){
      switch (  i) {
      case '1':
            vEnMantenimiento.setEstado("admitido");
           break;
      case '2':
           vEnMantenimiento.setEstado("reparacion");
           break;
      case '3':
           vEnMantenimiento.setEstado("prueba");
           break;
      
      default:
           System.out.println("error" );
           break;
      }
            
            
            }
        }
    }
    /**
     * Cambia el estado del vehiculo pasado por parametro a "listo", y lo agrega a la lista de vehiculos por entregar
     * @param vehiculo Recibe un vehiculo
     */
    public void darDeAlta(String usurario){
        for(Vehiculo vehiculoAlta: vehiculosMantenimiento ){
            
            if(vehiculoAlta.getDueno().equals(usuario)){
                
                vehiculosMantenimiento.remove(vehiculoAlta);
                vehiculosPorEntregar.add(vehiculoAlta);
                vehiculoAlta.setEstado("Retirar de taller");
            }
        }
    }
    
    
    
    
    public void calcularPreciodeMantenimientoPreventivo(Solicitud s){
   
         
        double costo = 0.10* s.getVehiculo().kilometraje; 
    }
    public String calcularPreciodeMantenimienEmergente(String mensajePrecio){
   
        return  mensajePrecio;
    }
    public void asignarPrecioSolicitudMantenimiento(Solicitud s){
        if(s.getEstado().equalsIgnoreCase("Preventivo")) {this.calcularPreciodeMantenimientoPreventivo(s);}
        else{this.calcularPreciodeMantenimienEmergente(usuario);}
    
    }
    
    public ArrayList<String> getCertificacionesTecnicas() {
        return certificacionesTecnicas;
    }

    public void setCertificacionesTecnicas(ArrayList<String> certificacionesTecnicas) {
        this.certificacionesTecnicas = certificacionesTecnicas;
    }
    
}
