/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosStock;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosPorEntregar;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosCotizados;

/**
 *
 * @author Campoverde Diego
 */
public  class Supervisor extends Usuario {
    private ArrayList<String> certificacionesAcademicas = new ArrayList<>();
    private static ArrayList<Solicitud> solicitudesCompra = new ArrayList<>();
    
    /**
     * Constructor de la clase Supervisor
     * @param usuario Recibe el nombre de usuario
     * @param contrasena Recibe la contrasena del usuario
     * @param nombres Recibe los dos nombres del usuario
     * @param apellidos Recibe los dos apellidos del usuario
     * @param certificacionesAcademicas Recibe una lista de certificaciones academicas
     */
    public Supervisor(String usuario, String contrasena, String nombres, String apellidos, ArrayList<String> certificacionesAcademicas) {
        super(usuario, contrasena, nombres, apellidos);
        this.certificacionesAcademicas=certificacionesAcademicas;
    }

    public Supervisor() {
    }
    
    /**
     * 
     * @return se acepta o se rechaza solicitud de Compra buscando en lista de soliciudes por indice de soliciud
     */
    public  void  aprobarSolicitud(String usuario) throws IOException{
        for(Solicitud s :solicitudesCompra ){
        
            if (s.getCliente().getUsuario()==usuario){
            
             s.setEstado("Aprobado");
            
            
            vehiculosCotizados.remove(s.getVehiculo());
            vehiculosPorEntregar.add(s.getVehiculo());
            
        FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("src\\proyectopoo_concesionaria\\Mensajes.txt",true);
        pw = new PrintWriter(fichero);
        pw.println(s.getCliente().getUsuario()+";"+"Retirar auto de Taller"+";"+s.getVehiculo()+";compra");    
        fichero.close();   
            }
        }
       
      
    }
    
    
    // al momeno de invocar este ,etodo en el main se debe pedir al supervisosr que ingrese motivo de rechazo
      public  void rechazarSolicitud(String cedula, String mensajeMotivo) throws IOException{
      
          for(Solicitud s :solicitudesCompra ){
        
            if (s.getCliente().getCedula()==cedula){
            
             s.setEstado("Rechazado");
            
                    
        FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("src\\proyectopoo_concesionaria\\Mensajes.txt",true);
        pw = new PrintWriter(fichero);
        pw.print(s.getCliente()+";"+mensajeMotivo+";"+s.getVehiculo().toString()+";compra");    
        fichero.close();  
                
            }
        }
       
    }

      
     
    public ArrayList<String> getCertificacionesAcademicas() {
        return certificacionesAcademicas;
    }

    public void setCertificacionesAcademicas(ArrayList<String> certificacionesAcademicas) {
        this.certificacionesAcademicas = certificacionesAcademicas;
    }

    public static ArrayList<Solicitud> getSolicitudesCompra() {
        return solicitudesCompra;
    }

    public static void setSolicitudesCompra(ArrayList<Solicitud> solicitudesCompra) {
        Supervisor.solicitudesCompra = solicitudesCompra;
    }

    

}
