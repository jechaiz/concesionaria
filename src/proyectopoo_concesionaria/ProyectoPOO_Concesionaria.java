/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Julian Echaiz
 */
public class ProyectoPOO_Concesionaria {
    static Scanner sc = new Scanner(System.in);
    static ArrayList<Vehiculo> vehiculosStock = new ArrayList<>();
    static ArrayList<Vehiculo> vehiculosMantenimiento = new ArrayList<>();
    static ArrayList<Vehiculo> vehiculosPorEntregar = new ArrayList<>();
    static ArrayList<Vehiculo> vehiculosCotizados = new ArrayList<>();
    static ArrayList<Vehiculo> vehiculosComprados = new ArrayList<>();
    static Cliente clienteConectado = new Cliente();
    static Vendedor vendedorConectado = new Vendedor();
    static Supervisor supervisorConectado = new Supervisor();
    static JefeDeTaller jdtConectado = new JefeDeTaller();
    static ArrayList<String[]> mensajes  = new ArrayList <>(); 
    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        
        ProyectoPOO_Concesionaria main = new ProyectoPOO_Concesionaria();
        main.cargarInformacionVehiculos();
        main.menuUsuario();
        main.guardarInformacionVehiculos();
    }
    
    public void menuUsuario() throws IOException{
        String opcion="";
        while(!opcion.equals("3")){
              System.out.println("╔                   Menu                    ╗");
              System.out.println("║ 1. Crear usuario.                         ║");
              System.out.println("║ 2. Entrar Sistema Concesionaria.          ║");
              System.out.println("║ 3. Salir.                                 ║");
              System.out.println("╚                                           ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    crearUsuario();
                    break;
                case "2":
                    entrarSistemaConcesionaria();
                    break;
                case "3":
                    System.out.println("Saliendo del sistema.");
                default:
                    if(!("1".equals(opcion) || "2".equals(opcion) || "3".equals(opcion))){
                        System.out.println("Opcion no valida.");
                    }
                    
            }
        }
        sc.close();
    }
    
    public void crearUsuario() throws IOException {
        FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("src\\proyectopoo_concesionaria\\usuarios.txt",true);
        pw = new PrintWriter(fichero);
        System.out.println("Creacion de usuario.");
        System.out.println("Ingrese su tipo de usuario como se muestra entre parentesis respectivamente (cliente, vendedor, supervisor, jefedetaller).");
        String tipoUser = sc.next();
        System.out.println("Ingrese sus dos nombres: ");
        String nombres = sc.next();
        System.out.println("Ingrese sus dos apellidos: ");
        String apellidos = sc.next();
        System.out.print("Nombre de Usuario: ");
        String userName = sc.next();
        String contrasena ="";
        while(contrasena.length()<6){
            System.out.print("Contraseña(Minimo 6 caracteres): ");
            contrasena = sc.next();
        }
        if("cliente".equals(tipoUser)){
            System.out.println("Ingrese su numero de cedula: ");
            String ced = sc.next();
            System.out.println("Ingrese su ocupacion: ");
            String ocu = sc.next();
            System.out.println("Ingrese ingresos mensuales: ");
            double ing = sc.nextDouble();
            
            pw.println(tipoUser+";"+userName+";"+nombres+";"+apellidos+";"+contrasena+";"+ced+";"+ocu+";"+ing);
            fichero.close();
        }
        if("vendedor".equals(tipoUser)){
            System.out.println("Ingrese id interno: ");
            String id = sc.next();
            pw.println(tipoUser+";"+userName+";"+nombres+";"+apellidos+";"+contrasena+";"+id);
            fichero.close();
        }
        if("supervisor".equals(tipoUser) || "jefedetaller".equals(tipoUser)){
            
            ArrayList<String> cert = new ArrayList<>();
            String token = "y";
            while("y".equals(token)){
                System.out.println("Ingrese certificado tecnico: ");
                String c = sc.next();
                cert.add(c);
                token = "";
                while(!("y".equals(token) || "n".equals(token))){
                    System.out.println("Desea ingresar otro certificado? (y/n)");
                    token = sc.next();
                }
                
            }
            pw.println(tipoUser+";"+userName+";"+nombres+";"+apellidos+";"+contrasena);
            for(String c : cert){
                pw.print(";"+c);
            }
            fichero.close();
        }   
                     
        System.out.println("Cuenta creada exitosamente.");
        
    }
    
    public void entrarSistemaConcesionaria() throws FileNotFoundException, IOException{
        File archivoUsers = null;
        FileReader frUsers = null;
        BufferedReader brUsers = null;
        archivoUsers = new File("src\\proyectopoo_concesionaria\\usuarios.txt");
        frUsers = new FileReader(archivoUsers);
        brUsers = new BufferedReader(frUsers);
        String lineaU;
        String[] parts;
        System.out.println("Ingrese su usuario: ");
        String user = sc.nextLine();
        System.out.println("Ingrese su contrasena: ");
        String pass = sc.nextLine();
        while((lineaU=brUsers.readLine())!=null){
            parts = lineaU.split(";");
            if("cliente".equals(parts[0]) && parts[1].equals(user) && parts[4].equals(pass)){
                ArrayList<Vehiculo> v = new ArrayList();
                for(Vehiculo veh: vehiculosMantenimiento){
                    if(veh.getDueno().equals(user)){
                        v.add(veh);
                    }
                }
                for(Vehiculo veh: vehiculosPorEntregar){
                    if(veh.getDueno().equals(user)){
                        v.add(veh);
                    }
                }
                for(Vehiculo veh: vehiculosComprados){
                    if(veh.getDueno().equals(user)){
                        v.add(veh);
                    }
                }
                clienteConectado = new Cliente(parts[1],parts[2],parts[3],parts[4],parts[5],parts[6],Double.parseDouble(parts[7]));
                if(v.size()>0){
                    clienteConectado.setVehiculos(v);         
                }
                subMenuCliente();
            }
            
            if("vendedor".equals(parts[0]) && parts[1].equals(user) && parts[4].equals(pass)){
                vendedorConectado = new Vendedor(parts[1],parts[2],parts[3],parts[4],parts[5]);
                subMenuVendedor();
            }
            
            if("supervisor".equals(parts[0]) && parts[1].equals(user) && parts[4].equals(pass)){
                ArrayList<String> cert = new ArrayList<>();
                for(int i = 5; i==parts.length;i++){
                    cert.add(parts[i]);
                }
                Supervisor sl = new Supervisor(parts[1],parts[2],parts[3],parts[4],cert);
                subMenuSupervisor();
            }
            
            if("jefedetaller".equals(parts[0]) && parts[1].equals(user) && parts[4].equals(pass)){
                ArrayList<String> cert = new ArrayList<>();
                for(int i = 5; i==parts.length;i++){
                    cert.add(parts[i]);
                }
                JefeDeTaller jt = new JefeDeTaller(parts[1],parts[2],parts[3],parts[4],cert);
                subMenuJefeTaller();
            }
            
            
        }
        frUsers.close();
    
    }
    
    public void subMenuCliente(){
        
        Cliente cl = clienteConectado;
        if(cl.getVehiculos().isEmpty()){
        System.out.println("");
        String opcion="";
        while(!opcion.equals("4")){
              System.out.println("╔                Menu Cliente               ╗");
              System.out.println("║ 1. Consultar stock de vehiculos.          ║");
              System.out.println("║ 2. Solicitar cotizacion.                  ║");
              System.out.println("║ 3. Comprar vehiculo.                      ║");
              System.out.println("║ 4. Regresar.                              ║");
              System.out.println("║ Solicitudes de Compra:                    ║");
              System.out.println("╚                                           ╝");
              mostrarMensajeCliente(cl.getUsuario());
              mostrarMensajeCotizacion(cl.getUsuario());
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    cl.consultarStock();
                    break;
                case "2":
                    
                    break;
                case "3":
                    
                    
                default:
                    System.out.println("");
            }
        }
        }
        if(!(cl.getVehiculos().isEmpty())){
        System.out.println("");
        String opcion="";
        while(!opcion.equals("6")){
              System.out.println("╔                Menu Cliente               ╗");
              System.out.println("║ 1. Consultar stock de vehiculos.          ║");
              System.out.println("║ 2. Solicitar cotizacion.                  ║");
              System.out.println("║ 3. Comprar vehiculo.                      ║");
              System.out.println("║ 4. Solicitar Mantenimiento.               ║");
              System.out.println("║ 5. Consultar estado de Mantenimiento.     ║");
              System.out.println("║ 6. Regresar.                              ║");
              System.out.println("║ Solicitudes de Compra:                    ║");
              System.out.println("╚                                           ╝");
              mostrarMensajeCliente(cl.getUsuario());
              mostrarMensajeCotizacion(cl.getUsuario());
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    cl.consultarStock();
                    break;
                case "2":
                    
                    break;
                case "3":
                    
                    break;
                case "4":
                    
                    break;    
                case "5":
                    
                    break;
                case "6":
                    
                    break;
                default:
                    System.out.println("");
            }
        }
        }
    }
    
    public void subMenuVendedor(){
        System.out.println("");
        String opcion="";
        while(!opcion.equals("3")){
              System.out.println("╔               Menu Vendedor               ╗");
              System.out.println("║ 1. Consultar stock de vehiculos.          ║");
              System.out.println("║ 2. Revisar solicitudes de cotizacion.     ║");
              System.out.println("║ 3. Regresar.                              ║");
              System.out.println("╚                                           ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    
                    break;
                case "2":
                    
                    break;

                default:
                    System.out.println("");
            }
        }
    }
    
    public void subMenuSupervisor() throws IOException{
        System.out.println("");
        String opcion="";
        while(!opcion.equals("3")){
              System.out.println("╔             Menu Supervisor               ╗");
              System.out.println("║ 1. Aceptar Solicitud de Compra            ║");
              System.out.println("║ 2. Rechazar solicitud de Compra     .     ║");
              System.out.println("║ 3. Regresar.                              ║");
              System.out.println("╚                                           ║");
              System.out.println("╚  Solicitudes:                             ╝");
    
              
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1": 
                    System.out.println("Ingrese usuario de solicitud que desea aprobar");
                    String usuario = sc.nextLine();
                    supervisorConectado.aprobarSolicitud(usuario);
                    break;
                case "2":
                    System.out.println("Ingrese usuario de solicitud que desea rechazar");
                    String u = sc.nextLine();
                    System.out.println("Ingrese motivo de rechazo");
                    String m = sc.nextLine();
                    supervisorConectado.rechazarSolicitud(u, m);
                    break;

                default:
                    System.out.println("");
            }
        }
    }
    
    public void subMenuJefeTaller() throws IOException{
        
              System.out.println("");
        String opcion="";
        while(!opcion.equals("4")){
              System.out.println("╔             Menu Jefe Taller              ╗");
              System.out.println("║ 1. Entregar autos vendidos                ║");
              System.out.println("║ 2. Aceptar solicitudes Mantenimiento.     ║");
              System.out.println("║ 3. Dar de alta.                           ║");
              System.out.println("║ 4. Regresar.                              ║");
              System.out.println("╚                                           ║");
              System.out.println("╚  Solicitudes:                             ╝");
    
              
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1": 
                    for(Vehiculo v: vehiculosPorEntregar){
                        jdtConectado.entregarVehiculo(v);
                    }
                    break;
                case "2":
                    System.out.println("Ingrese usuario de solicitud que desea aceptar");
                    String u = sc.nextLine();
                    jdtConectado.aprobarSolicitudDeMantenimiento(u);
                    break;
                case "3":
                    System.out.println("Ingrese nombre del Duennio del auto que desea dar de alta:");
                    String nombre = sc.nextLine();
                    jdtConectado.darDeAlta(nombre);
                    break;
                default:
                    System.out.println("");
            }
        }
        
    }
    
    public void cargarInformacionVehiculos() throws FileNotFoundException, IOException{
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        archivo = new File("src\\proyectopoo_concesionaria\\vehiculos.txt");
        fr = new FileReader(archivo);
        br = new BufferedReader(fr);
        String line;
        String[] v;
        while((line=br.readLine())!=null){
            v=line.split(";");
            if("automovil".equals(v[0])){
                int anio = Integer.parseInt(v[3]);
                int llantas = Integer.parseInt(v[4]);
                int km = Integer.parseInt(v[6]);
                int asientos = Integer.parseInt(v[7]);
                if("stock".equals(v[5])){
                    vehiculosStock.add(new Automovil(v[1],v[2],anio,llantas,v[5],km,asientos,Boolean.parseBoolean(v[8]),Boolean.parseBoolean(v[9]),v[10]));
                }
                if("comprado".equals(v[5])){
                    vehiculosComprados.add(new Automovil(v[1],v[2],anio,llantas,v[5],km,asientos,Boolean.parseBoolean(v[8]),Boolean.parseBoolean(v[9]),v[10]));
                }
                if("admitido".equals(v[5])||"reparacion".equals(v[5]) || "prueba".equals(v[5])){
                    vehiculosMantenimiento.add(new Automovil(v[1],v[2],anio,llantas,v[5],km,asientos,Boolean.parseBoolean(v[8]),Boolean.parseBoolean(v[9]),v[10]));
                }
                if("por entregar".equals(v[5])){
                    vehiculosPorEntregar.add(new Automovil(v[1],v[2],anio,llantas,v[5],km,asientos,Boolean.parseBoolean(v[8]),Boolean.parseBoolean(v[9]),v[10]));
                }
                if("cotizado".equals(v[5])){
                    vehiculosCotizados.add(new Automovil(v[1],v[2],anio,llantas,v[5],km,asientos,Boolean.parseBoolean(v[8]),Boolean.parseBoolean(v[9]),v[10]));
                }
            }
            if("camion".equals(v[0])){
                if("stock".equals(v[5])){
                    vehiculosStock.add(new Camion(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Double.parseDouble(v[7]),v[8]));
                }
                if("comprado".equals(v[5])){
                    vehiculosComprados.add(new Camion(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Double.parseDouble(v[7]),v[8]));
                }
                if("admitido".equals(v[5])||"reparacion".equals(v[5]) || "prueba".equals(v[5])){
                    vehiculosMantenimiento.add(new Camion(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Double.parseDouble(v[7]),v[8]));
                }
                if("por entregar".equals(v[5])){
                    vehiculosPorEntregar.add(new Camion(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Double.parseDouble(v[7]),v[8]));
                }
                if("cotizado".equals(v[5])){
                    vehiculosCotizados.add(new Camion(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Double.parseDouble(v[7]),v[8]));
                }
            }
            if("motocicleta".equals(v[0])){
                if("stock".equals(v[5])){
                    vehiculosStock.add(new Motocicleta(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),v[7],v[8]));
                }
                if("comprado".equals(v[5])){
                    vehiculosComprados.add(new Motocicleta(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),v[7],v[8]));
                }
                if("admitido".equals(v[5])||"reparacion".equals(v[5]) || "prueba".equals(v[5])){
                    vehiculosMantenimiento.add(new Motocicleta(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),v[7],v[8]));
                }
                if("por entregar".equals(v[5])){
                    vehiculosPorEntregar.add(new Motocicleta(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),v[7],v[8]));
                }
                if("cotizado".equals(v[5])){
                    vehiculosCotizados.add(new Motocicleta(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),v[7],v[8]));
                }
            }
            if("tractor".equals(v[0])){
                if("stock".equals(v[5])){
                    vehiculosStock.add(new Tractor(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Boolean.parseBoolean(v[7]),v[8],v[9]));
                }
                if("comprado".equals(v[5])){
                    vehiculosComprados.add(new Tractor(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Boolean.parseBoolean(v[7]),v[8],v[9]));
                }
                if("admitido".equals(v[5])||"reparacion".equals(v[5]) || "prueba".equals(v[5])){
                    vehiculosMantenimiento.add(new Tractor(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Boolean.parseBoolean(v[7]),v[8],v[9]));
                }
                if("por entregar".equals(v[5])){
                    vehiculosPorEntregar.add(new Tractor(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Boolean.parseBoolean(v[7]),v[8],v[9]));
                }
                if("cotizado".equals(v[5])){
                    vehiculosCotizados.add(new Tractor(v[1],v[2],Integer.parseInt(v[3]),Integer.parseInt(v[4]),v[5],Integer.parseInt(v[6]),Boolean.parseBoolean(v[7]),v[8],v[9]));
                }
            }
        }
        fr.close();
    }
    
    public void guardarInformacionVehiculos() throws IOException{
        FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("src\\proyectopoo_concesionaria\\vehiculos.txt",false);
        pw = new PrintWriter(fichero);
        for(Vehiculo veh: vehiculosStock){
            if(veh instanceof Automovil){
                pw.println("automovil;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Automovil) veh).getNumero_asientos()+";"+((Automovil) veh).isConvertible()+";"+((Automovil) veh).isCam_retro()+";"+veh.getDueno());
            }
            if(veh instanceof Motocicleta){
                pw.println("motocicleta;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Motocicleta) veh).getCategoria()+";"+veh.getDueno());
            }
            if(veh instanceof Camion){
                pw.println("camion;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Camion) veh).getCapacidadcarga()+";"+veh.getDueno());
            }
            if(veh instanceof Tractor){
                pw.println("tractor;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Tractor) veh).isUsoagricola()+";"+((Tractor) veh).getTipoTransmision()+";"+veh.getDueno());
            }
        }
        for(Vehiculo veh: vehiculosMantenimiento){
            if(veh instanceof Automovil){
                pw.println("automovil;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Automovil) veh).getNumero_asientos()+";"+((Automovil) veh).isConvertible()+";"+((Automovil) veh).isCam_retro()+";"+veh.getDueno());
            }
            if(veh instanceof Motocicleta){
                pw.println("motocicleta;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Motocicleta) veh).getCategoria()+";"+veh.getDueno());
            }
            if(veh instanceof Camion){
                pw.println("camion;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Camion) veh).getCapacidadcarga()+";"+veh.getDueno());
            }
            if(veh instanceof Tractor){
                pw.println("tractor;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Tractor) veh).isUsoagricola()+";"+((Tractor) veh).getTipoTransmision()+";"+veh.getDueno());
            }
        }
        for(Vehiculo veh: vehiculosPorEntregar){
            if(veh instanceof Automovil){
                pw.println("automovil;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Automovil) veh).getNumero_asientos()+";"+((Automovil) veh).isConvertible()+";"+((Automovil) veh).isCam_retro()+";"+veh.getDueno());
            }
            if(veh instanceof Motocicleta){
                pw.println("motocicleta;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Motocicleta) veh).getCategoria()+";"+veh.getDueno());
            }
            if(veh instanceof Camion){
                pw.println("camion;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Camion) veh).getCapacidadcarga()+";"+veh.getDueno());
            }
            if(veh instanceof Tractor){
                pw.println("tractor;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Tractor) veh).isUsoagricola()+";"+((Tractor) veh).getTipoTransmision()+";"+veh.getDueno());
            }
        }
        for(Vehiculo veh: vehiculosComprados){
            if(veh instanceof Automovil){
                pw.println("automovil;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Automovil) veh).getNumero_asientos()+";"+((Automovil) veh).isConvertible()+";"+((Automovil) veh).isCam_retro()+";"+veh.getDueno());
            }
            if(veh instanceof Motocicleta){
                pw.println("motocicleta;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Motocicleta) veh).getCategoria()+";"+veh.getDueno());
            }
            if(veh instanceof Camion){
                pw.println("camion;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Camion) veh).getCapacidadcarga()+";"+veh.getDueno());
            }
            if(veh instanceof Tractor){
                pw.println("tractor;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Tractor) veh).isUsoagricola()+";"+((Tractor) veh).getTipoTransmision()+";"+veh.getDueno());
            }
        }
        for(Vehiculo veh: vehiculosCotizados){
            if(veh instanceof Automovil){
                pw.println("automovil;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Automovil) veh).getNumero_asientos()+";"+((Automovil) veh).isConvertible()+";"+((Automovil) veh).isCam_retro()+";"+veh.getDueno());
            }
            if(veh instanceof Motocicleta){
                pw.println("motocicleta;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Motocicleta) veh).getCategoria()+";"+veh.getDueno());
            }
            if(veh instanceof Camion){
                pw.println("camion;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Camion) veh).getCapacidadcarga()+";"+veh.getDueno());
            }
            if(veh instanceof Tractor){
                pw.println("tractor;"+veh.getMarca()+";"+veh.getModelo()+";"+veh.getAnio_fabricacion()+";"+veh.getNumero_llantas()+";"+veh.getEstado()+";"+veh.getKilometraje()+";"+((Tractor) veh).isUsoagricola()+";"+((Tractor) veh).getTipoTransmision()+";"+veh.getDueno());
            }
        }
        fichero.close();
    }
    
    public void cargarMensaje() throws FileNotFoundException, IOException{
    
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        archivo = new File("src\\proyectopoo_concesionaria\\Mensajes.txt");
        fr = new FileReader(archivo);
        br = new BufferedReader(fr);
        String line;
        String[] v;
        while((line=br.readLine())!=null){
            v=line.split(";");
            mensajes.add(v);
        }
        fr.close();
    }
    
    public void mostrarMensajeCliente(String usuario){
        System.out.println("Vehiculos adquiridos:");
        for(String[] s : mensajes){
            if(s[0].equals(usuario)&&"compra".equals(s[3])){
                int i =1 ;
                
                System.out.println("Solicitud "+i+": "+s[1]+" Vehiculo Adquirido: "+s[2]);
                
                i++;
            }
            
        }
    
    
    }
        public void mostrarMensajeCotizacion(String usuario){
        System.out.println("Cotizaciones aprobadas: ");
        for(String[] s : mensajes){
            if(s[0].equals(usuario)&&"cotizacion".equals(s[3])){
                int i =1 ;
                
                System.out.println("Solicitud "+i+": "+s[1]+" Vehiculo cotizado: "+s[2]);
                
                i++;
            }
            
        }
    
    
    }
    
}
