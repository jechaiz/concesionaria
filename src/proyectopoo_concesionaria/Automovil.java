/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

/**
 *
 * @author use
 */
public class Automovil extends Vehiculo {
    
    private int numero_asientos;
    private boolean convertible;
    private boolean cam_retro;
    
    

    public Automovil(String marca, String modelo, int anio_fabricacion, int numero_llantas,String estado, int km,int numero_asientos,boolean convertible, boolean cam_retro, String dueno) {
        super(marca, modelo, anio_fabricacion, numero_llantas,estado,km,dueno);
        this.numero_asientos=numero_asientos;
        this.convertible=convertible;
        this.cam_retro=cam_retro;
    }

    public int getNumero_asientos() {
        return numero_asientos;
    }

    public void setNumero_asientos(int numero_asientos) {
        this.numero_asientos = numero_asientos;
    }

    public boolean isConvertible() {
        return convertible;
    }

    public void setConvertible(boolean convertible) {
        this.convertible = convertible;
    }

    public boolean isCam_retro() {
        return cam_retro;
    }

    public void setCam_retro(boolean cam_retro) {
        this.cam_retro = cam_retro;
    }

    @Override
    public String toString() {
        return "Automovil{" + "numero_asientos=" + numero_asientos + ", convertible=" + convertible + ", cam_retro=" + cam_retro + '}';
    }


    
    
        
  
}
