/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.util.Objects;

/**
 *
 * @author use
 */
public abstract class Vehiculo {
    
    protected String marca;
    protected String modelo;
    protected int anio_fabricacion;
    protected int numero_llantas;
    protected String estado;
    protected int kilometraje;
    protected String dueno;
    
    public Vehiculo(String marca,String modelo, int anio_fabricacion, int numero_llantas, String estado, int km, String dueno){
    
        this.marca=marca;
        this.modelo=modelo;
        this.anio_fabricacion=anio_fabricacion;
        this.numero_llantas=numero_llantas;
        this.estado=estado;
        this.kilometraje=km;
        this.dueno=dueno;
    
    }
    
    public String getMarca(){
        return this.marca;
    }
    public void setMarca(String marca){
        this.marca=marca;
    }
    public String getModelo(){
        return this.modelo;
    }
    public void setModelo(String modelo){
        this.modelo=modelo;
    }
    public int getAnio_fabricacion(){
        return this.anio_fabricacion;
    }
    public void setAnio_fabricacion(int anio_fabricacion){
        this.anio_fabricacion=anio_fabricacion;
    }
    public int getNumero_llantas(){
        return this.numero_llantas;
    }
    public void setNumero_llantas(int numero_llantas){
        this.numero_llantas=numero_llantas;
    }
    public String getEstado(){
        return this.estado;
    }
    public void setEstado(String estado){
        this.estado=estado;
    }

    public int getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(int kilometraje) {
        this.kilometraje = kilometraje;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }
    
    public boolean estaEnMantenimiento(){
    return false;
    }
    
    public void estadoDeMantenimiento(){
    
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (this.anio_fabricacion != other.anio_fabricacion) {
            return false;
        }
        if (this.numero_llantas != other.numero_llantas) {
            return false;
        }
        if (this.kilometraje != other.kilometraje) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        if (!Objects.equals(this.modelo, other.modelo)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "marca=" + marca + ", modelo=" + modelo + ", anio_fabricacion=" + anio_fabricacion + ", numero_llantas=" + numero_llantas + ", estado=" + estado + ", kilometraje=" + kilometraje + ", dueno=" + dueno + '}';
    }
    
    
}
