/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

/**
 *
 * @author use
 */
public abstract class Usuario {
    
    protected String usuario;
    protected String nombres;
    protected String apellidos;
    protected String contrasena;
    
    public Usuario(String usuario, String nombres, String apellidos, String contrasena){
        this.usuario=usuario;
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.contrasena=contrasena;
    }

    public Usuario() {
    }
    
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
    public String getNombres(){
        return this.nombres;
    }
    public void setNombres(String nombres){
        this.nombres=nombres;
    }
    public String getApellidos(){
        return this.apellidos;
    }
    public void setApellidos(String apellidos){
        this.apellidos=apellidos;
    }
    public String setContrasena(){
        return this.contrasena;
    }
    public void setContrasena(String contrasena){
        this.contrasena=contrasena;
    }
}
