/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.util.ArrayList;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.sc;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosStock;
import static proyectopoo_concesionaria.Vendedor.solicitudesCotizacion;

/**
 *
 * @author Julian Echaiz
 */
public class Cliente extends Usuario {
    
    private ArrayList<Vehiculo> vehiculos;
    private String cedula;
    private String ocupacion;
    private double ingresosMensuales;
    
   
    
    public Cliente(String usuario, String contrasena, String nombres, String apellidos, String cedula, String ocupacion, double ingresosMensuales) {
        super(usuario, contrasena, nombres, apellidos);
        this.vehiculos= new ArrayList<>();
        this.cedula=cedula;
        this.ocupacion=ocupacion;
        this.ingresosMensuales=ingresosMensuales;
    }

    public Cliente() {

    }
    
    
    /**
     * Muestra la marca, modelo y ano de fabricacion de cada vehiculo disponible para la venta
     */
    public void consultarStock(){
        for(Vehiculo veh: vehiculosStock){
            
            System.out.println("Vehiculo disponible #"+(vehiculosStock.indexOf(veh)+1));
            System.out.println("Marca: "+veh.getMarca()+", Modelo: "+veh.getModelo()+", Año de fabricacion: "+veh.getAnio_fabricacion()+".");
            System.out.println("\n Presione enter para coninuar.");
            String ss = sc.next();
            
        }
    }
    /**
     * solicita una cotizacion a un vendedor aleatorio para poder ver el precio y las especificaciones del vehiculo
     */
    public void solicitarCotizacion(Cliente cl){
        System.out.println("Revise en la lista de stock el numero del vehiculo que desea cotizar.");
        int idx = sc.nextInt();
        Solicitud s= new Solicitud("cotizado","espera",vehiculosStock.get(idx),cl);
        solicitudesCotizacion.add(s);
        
    }
    /**
     * Solo puede comprar() si ya ha usado solicitarCotizacion()
     * Permite comprar o rechazar el vehiculo elegido, si elige comprarlo se envia una solicutud de compra al supervisor y cambia el estado del vehiculo a solicitado
     *   
     */
    public void comprar(){
        
    }
    /**
     * Verifica si el cliente ya ha comprado algun vehiculo en la consecionaria antes
     * @return Devuelve true si el cliente ya ha comprado algun vehiculo en la consecionaria antes
     */
    public boolean verficarCliente(){
        return true;
    }
    /**
     * Imprime una cadena de caracteres indicando el progreso del mantenimiento de el o los vehiculos del cliente, si el cliente no tiene ningun vehiculo en mantemiento se imprime "No existen vehiculos en mantenimiento"
     */
    public void consultarProgreso(){
        
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public double getIngresosMensuales() {
        return ingresosMensuales;
    }

    public void setIngresosMensuales(double ingresosMensuales) {
        this.ingresosMensuales = ingresosMensuales;
    }

    
    
}
