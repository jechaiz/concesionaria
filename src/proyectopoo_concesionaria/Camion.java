/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

/**
 *
 * @author use
 */
public class Camion extends Vehiculo {
    
    private double capacidadcarga;
    private int numero_ejes;

    public Camion(String marca, String modelo, int anio_fabricacion, int numero_llantas,String estado, int km, double capacidadcarga,String dueno) {
        super(marca, modelo, anio_fabricacion, numero_llantas,estado,km,dueno);
        this.numero_ejes=numero_llantas/2;
        this.capacidadcarga=capacidadcarga;
    }

    public double getCapacidadcarga() {
        return capacidadcarga;
    }

    public void setCapacidadcarga(double capacidadcarga) {
        this.capacidadcarga = capacidadcarga;
    }

    @Override
    public int getNumero_llantas() {
        return numero_llantas;
    }

    @Override
    public void setNumero_llantas(int numero_llantas) {
        this.numero_llantas = numero_llantas;
    }

    public int getNumero_ejes() {
        return numero_ejes;
    }

    public void setNumero_ejes(int numero_ejes) {
        this.numero_ejes = numero_ejes;
    }

    @Override
    public String toString() {
        return "Camion{" + "capacidadcarga=" + capacidadcarga + ", numero_ejes=" + numero_ejes + '}';
    }
    
    
    
}
