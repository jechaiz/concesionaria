/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

/**
 *
 * @author use
 */
public class Motocicleta extends Vehiculo {
    
    public String categoria;
    
    

    public Motocicleta(String marca, String modelo, int anio_fabricacion, int numero_llantas, String estado, int km, String categoria, String dueno) {
        super(marca, modelo, anio_fabricacion, numero_llantas,estado,km,dueno);
        this.categoria=categoria;
        
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return "Motocicleta{" + "categoria=" + categoria + '}';
    }
    
    
}
