/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosCotizados;
import static proyectopoo_concesionaria.ProyectoPOO_Concesionaria.vehiculosStock;

/**
 *
 * @author Julian Echaiz
 */
public class Vendedor extends Usuario{
    
    private String id;
    public static ArrayList<Solicitud> solicitudesCotizacion;
    
    public Vendedor(String usuario, String contrasena, String nombres, String apellidos, String id) {
        super(usuario, contrasena, nombres, apellidos);
        this.solicitudesCotizacion= new ArrayList<>();
        this.id=id;
    }

    public Vendedor() {
    }
    
    
   
    public void aprobarCotizacion(String usuario) throws IOException{
        for (Solicitud s :solicitudesCotizacion){
        if(s.getCliente().getUsuario()== usuario){
        
            s.setEstado("Aprobado");
            vehiculosStock.remove(s.getVehiculo());
            vehiculosCotizados.add(s.getVehiculo());
               FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("src\\proyectopoo_concesionaria\\Mensajes.txt",true);
        pw = new PrintWriter(fichero);
        pw.println(s.getCliente().getUsuario()+";"+"Cotizacion Aceptada"+";"+s.getVehiculo()+" Precio: $15000;cotizacion");    
        fichero.close();  
        }
        
        
        }
    }
    
    /**
     * 
     * @return Si la solicitud de cotizacion que envio el cliente fue aprobada devuelve una cadena de caracteres con la informacion de precio y especificaciones del vehiculo solicitado. Si la solicitud del cliente no fue aprobada se debe mostrar un mensaje al cliente con la justificacion de porque no fue aprobada su solicitud 
     */
    public String estadoDeSolicitud(){
        return "";
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Solicitud> getSolicitudesCotizacion() {
        return solicitudesCotizacion;
    }

    public void setSolicitudesCotizacion(ArrayList<Solicitud> solicitudesCotizacion) {
        this.solicitudesCotizacion = solicitudesCotizacion;
    }
    
    
}