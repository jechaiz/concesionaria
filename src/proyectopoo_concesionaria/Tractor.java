/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo_concesionaria;

/**
 *
 * @author use
 */
public class Tractor extends Vehiculo {
    
    public boolean usoagricola;
    public String tipoTransmision;

    public Tractor(String marca, String modelo, int anio_fabricacion, int numero_llantas, String estado, int km, boolean usoagricola, String tipoTransmision, String dueno) {
        super(marca, modelo, anio_fabricacion, numero_llantas,estado,km,dueno);
        this.usoagricola=usoagricola;
        this.tipoTransmision=tipoTransmision;
    }

    public boolean isUsoagricola() {
        return usoagricola;
    }

    public void setUsoagricola(boolean usoagricola) {
        this.usoagricola = usoagricola;
    }

    public String getTipoTransmision() {
        return tipoTransmision;
    }

    public void setTipoTransmision(String tipoTransmision) {
        this.tipoTransmision = tipoTransmision;
    }

    @Override
    public String toString() {
        return "Tractor{" + "usoagricola=" + usoagricola + ", tipoTransmision=" + tipoTransmision + '}';
    }
    
    
    
}
